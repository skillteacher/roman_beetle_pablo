using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    [SerializeField] private float jumpForce = 10f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private string groundLayer = "Ground";
    
    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrouned;


    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        SwitchAnimation(playerInput);
        Flip(playerInput);
        isGrouned = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if (Input.GetKeyDown(jumpButton) && isGrouned)
        {
            Jump();
        }
    }

    private void Move(float direction)
    {
        playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
    }

    private void Jump()
    {
        Vector2 jumpVector2 = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity = jumpVector2;
    }


    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Run", playerInput != 0);
    }


    private void Flip(float playerInput)
    {
        if(playerInput < 0)
        {
            playerSpriteRenderer.flipX = true;
        }
        if (playerInput > 0)
        {
            playerSpriteRenderer.flipX = false;
        }
    }
}
